# Participa Som Connexió Provisioning

Scripts to manage the users of participa Som Connexió servers.

## Requirements

### Python version

We use [Pyenv](https://github.com/pyenv/pyenv) to fix the Python version and the virtualenv to develop the package.

You need to:

* Install and configure [`pyenv`](https://github.com/pyenv/pyenv)
* Install and configure [`pyenv-virtualenvwrapper`](https://github.com/pyenv/pyenv-virtualenvwrapper)
* Install the required Python version:

```
$ pyenv install 3.8.12
```

* Create the virtualenv:

```
$ pyenv virtualenv 3.8.12 sc-participa-provisioning
```

### Python packages requirements

Install the Python packages in the virtual environment:

```
$ pyenv exec pip install -r requirements.txt
```
After installing Ansible, we need to download and install the project dependencies:

```
$ pyenv exec ansible-galaxy install -r requirements.yml
```


## Usage

### Create System Administrators users - playbooks/sys_admins.yml

This playbook uses Coopdevs' [`sys-admins`role](https://github.com/coopdevs/sys-admins-role) to manage the system administrators users.

The first time you run it against a brand new host you need to run it as `root` user. You'll also need passwordless SSH access to the root user.

After doing so, you can substitute `root` from the command for your personal user configured as SysAdmin.

```
$ pyenv exec ansible-playbook playbooks/sys_admins.yml --limit HOSTGROUP -u root
```

## Linting

To check the correct ansible structure and YAML syntax run the following commands, executing linters ([Ansible Lint](https://docs.ansible.com/ansible-lint/index.html), [yamllint](https://github.com/adrienverge/yamllint)) that will pass without warnings if everything is ok:

```
$ ansible-lint playbooks/*.yml
$ yamllint **/*.yml
```
